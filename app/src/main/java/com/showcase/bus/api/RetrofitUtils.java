package com.showcase.bus.api;


import com.showcase.bus.BuildConfig;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtils {

    public static <T> T getService(Class<T> cls) {
        return getRetrofit().create(cls);
    }

    private static Retrofit getRetrofit() {
        OkHttpClient.Builder httpClientBuilder = new OkHttpClient.Builder()
                .hostnameVerifier((hostname, session) -> true)
                .addInterceptor(chain -> chain.proceed(chain.request().newBuilder()
                        .addHeader("X-Api-Authentication", BuildConfig.API_AUTHENTICATION_TOKEN).build()));
        return new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();

    }
}