package com.showcase.bus.api;


import com.showcase.bus.R;

public enum StationsEnum {
    BERLIN_ZOB(1, R.string.berlinZob),
    MUNICH_ZOB(10, R.string.munichZob);

    private final long mStationId;
    private final int mStationTitleId;

    StationsEnum(long stationId, int stationTitleId) {
        this.mStationId = stationId;
        this.mStationTitleId = stationTitleId;
    }

    public long getStationId() {
        return mStationId;
    }

    public int getStationTitleId() {
        return mStationTitleId;
    }
}
