
package com.showcase.bus.api.pojo;

import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class Timetable {

    List<BusRoute> arrivals = new ArrayList<>();
    List<BusRoute> departures = new ArrayList<BusRoute>();

    /**
     * @return The arrivals
     */
    public List<BusRoute> getArrivals() {
        return arrivals;
    }

    /**
     * @param arrivals The arrivals
     */
    public void setArrivals(List<BusRoute> arrivals) {
        this.arrivals = arrivals;
    }

    /**
     * @return The departures
     */
    public List<BusRoute> getDepartures() {
        return departures;
    }

    /**
     * @param departures The departures
     */
    public void setDepartures(List<BusRoute> departures) {
        this.departures = departures;
    }

}
