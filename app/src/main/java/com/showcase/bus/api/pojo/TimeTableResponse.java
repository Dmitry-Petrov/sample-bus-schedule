package com.showcase.bus.api.pojo;

import org.parceler.Parcel;

@Parcel
public class TimeTableResponse {

    Timetable timetable;

    /**
     * @return The timetable
     */
    public Timetable getTimetable() {
        return timetable;
    }

    /**
     * @param timetable The timetable
     */
    public void setTimetable(Timetable timetable) {
        this.timetable = timetable;
    }


}
