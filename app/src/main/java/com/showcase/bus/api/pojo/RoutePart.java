package com.showcase.bus.api.pojo;

import org.parceler.Parcel;

@Parcel
public class RoutePart {

    Integer id;
    String name;
    String address;
    String full_address;
    Coordinates coordinates;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return The full_address
     */
    public String getFull_address() {
        return full_address;
    }

    /**
     * @param full_address The full_address
     */
    public void setFull_address(String full_address) {
        this.full_address = full_address;
    }

    /**
     * @return The coordinates
     */
    public Coordinates getCoordinates() {
        return coordinates;
    }

    /**
     * @param coordinates The coordinates
     */
    public void setCoordinates(Coordinates coordinates) {
        this.coordinates = coordinates;
    }

}
