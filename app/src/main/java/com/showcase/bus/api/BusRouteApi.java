package com.showcase.bus.api;

import com.showcase.bus.api.pojo.TimeTableResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Path;

public interface BusRouteApi {

    @GET("/mobile/v1/network/station/{station_id}/timetable")
    Single<TimeTableResponse> loadBusScheduleForStation(@Path("station_id") long stationId);
}
