package com.showcase.bus.api.pojo;

import org.parceler.Parcel;

@Parcel
public class Delay {

    Long timestamp;
    String tz;

    /**
     * @return The timestamp
     */
    public Long getTimestamp() {
        return timestamp;
    }

    /**
     * @param timestamp The timestamp
     */
    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    /**
     * @return The tz
     */
    public String getTz() {
        return tz;
    }

    /**
     * @param tz The tz
     */
    public void setTz(String tz) {
        this.tz = tz;
    }

}
