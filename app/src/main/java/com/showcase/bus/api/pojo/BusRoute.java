package com.showcase.bus.api.pojo;


import org.parceler.Parcel;

import java.util.ArrayList;
import java.util.List;

@Parcel
public class BusRoute {

    String through_the_stations;
    Datetime datetime;
    String line_direction;
    List<RoutePart> route = new ArrayList<RoutePart>();
    String line_code;
    String direction;

    /**
     * @return The through_the_stations
     */
    public String getThrough_the_stations() {
        return through_the_stations;
    }

    /**
     * @param through_the_stations The through_the_stations
     */
    public void setThrough_the_stations(String through_the_stations) {
        this.through_the_stations = through_the_stations;
    }


    /**
     * @return The line_direction
     */
    public String getLine_direction() {
        return line_direction;
    }

    /**
     * @param line_direction The line_direction
     */
    public void setLine_direction(String line_direction) {
        this.line_direction = line_direction;
    }

    /**
     * @return The route
     */
    public List<RoutePart> getRoute() {
        return route;
    }

    /**
     * @param route The route
     */
    public void setRoute(List<RoutePart> route) {
        this.route = route;
    }

    /**
     * @return The line_code
     */
    public String getLine_code() {
        return line_code;
    }

    /**
     * @param line_code The line_code
     */
    public void setLine_code(String line_code) {
        this.line_code = line_code;
    }

    /**
     * @return The direction
     */
    public String getDirection() {
        return direction;
    }

    /**
     * @param direction The direction
     */
    public void setDirection(String direction) {
        this.direction = direction;
    }

    public Datetime getDatetime() {
        return datetime;
    }

    public void setDatetime(Datetime datetime) {
        this.datetime = datetime;
    }

}
