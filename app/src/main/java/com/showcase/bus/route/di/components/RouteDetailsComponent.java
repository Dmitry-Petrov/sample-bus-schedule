package com.showcase.bus.route.di.components;

import com.showcase.bus.di.qualifiers.PerActivity;
import com.showcase.bus.route.RouteDetailsActivity;
import com.showcase.bus.route.di.modules.RouteDetailsModule;

import dagger.Subcomponent;

@Subcomponent(modules = {RouteDetailsModule.class})
@PerActivity
public interface RouteDetailsComponent {

    void inject(RouteDetailsActivity routeDetailsActivity);
}
