package com.showcase.bus.route.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.showcase.bus.R;
import com.showcase.bus.api.pojo.RoutePart;

import java.util.ArrayList;
import java.util.List;


public class BusRoutePartAdapter extends RecyclerView.Adapter<BusRoutePartViewHolder> {

    private List<RoutePart> mRouteParts = new ArrayList<>();

    public void setData(List<RoutePart> routeParts) {
        this.mRouteParts = routeParts;
        notifyDataSetChanged();
    }

    @Override
    public BusRoutePartViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        return new BusRoutePartViewHolder(inflater.inflate(R.layout.route_part_item, parent, false));
    }

    @Override
    public void onBindViewHolder(final BusRoutePartViewHolder holder, final int position) {
        RoutePart routePart = mRouteParts.get(position);
        holder.mRoutePartNumber.setText(String.valueOf(position + 1));
        holder.mRoutePartAddress.setText(String.format("%s, %s", routePart.getName(), routePart.getAddress()));
    }

    @Override
    public int getItemCount() {
        return mRouteParts.size();
    }

}
