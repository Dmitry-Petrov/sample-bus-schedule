package com.showcase.bus.route.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.showcase.bus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class BusRoutePartViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.route_part_number)
    TextView mRoutePartNumber;

    @BindView(R.id.route_part_address)
    TextView mRoutePartAddress;

    BusRoutePartViewHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

}