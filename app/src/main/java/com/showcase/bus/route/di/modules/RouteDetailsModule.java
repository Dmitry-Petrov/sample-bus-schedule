package com.showcase.bus.route.di.modules;

import android.content.Context;

import com.showcase.bus.route.RouteDetailsActivityPresenter;

import dagger.Module;
import dagger.Provides;

@Module
public class RouteDetailsModule {

    @Provides
    RouteDetailsActivityPresenter provideRouteDetailsActivityPresenter(Context appContext) {
        return new RouteDetailsActivityPresenter(appContext);
    }
}
