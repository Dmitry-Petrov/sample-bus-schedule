package com.showcase.bus.route;

import android.content.Context;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.showcase.bus.api.pojo.BusRoute;
import com.showcase.bus.api.pojo.Coordinates;
import com.showcase.bus.api.pojo.RoutePart;
import com.showcase.bus.utils.DimensionUtils;

public class RouteDetailsActivityPresenter {

    Context mContext;

    public RouteDetailsActivityPresenter(Context context) {
        mContext = context;
    }

    CameraUpdate buildMarkers(BusRoute busRoute, GoogleMap map) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (RoutePart routePart : busRoute.getRoute()) {
            Coordinates coordinates = routePart.getCoordinates();
            Marker marker = map.addMarker(
                    new MarkerOptions()
                            .position(new LatLng(coordinates.getLatitude(), coordinates.getLongitude()))
                            .title(routePart.getName())
                            .snippet(routePart.getFull_address()));
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        return CameraUpdateFactory.newLatLngBounds(bounds, DimensionUtils.fromDpToPixels(mContext, 50));
    }

}
