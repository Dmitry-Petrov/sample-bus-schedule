package com.showcase.bus.route;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.Toolbar;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.showcase.bus.MainApplication;
import com.showcase.bus.R;
import com.showcase.bus.api.pojo.BusRoute;
import com.showcase.bus.route.adapter.BusRoutePartAdapter;
import com.showcase.bus.route.di.modules.RouteDetailsModule;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RouteDetailsActivity extends Activity implements OnMapReadyCallback {

    private static final String BUS_ROUTE_ARG = "BUS_ROUTE_ARG";

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.route_parts_recycler_view)
    RecyclerView mRoutePartsRecyclerView;

    @BindView(R.id.map_view)
    MapView mapView;

    @Inject
    RouteDetailsActivityPresenter mPresenter;

    private BusRoute mBusRoute;

    public static void startActivity(Activity activity, BusRoute busRoute) {
        Intent intent = new Intent(activity, RouteDetailsActivity.class);
        intent.putExtra(BUS_ROUTE_ARG, Parcels.wrap(busRoute));
        activity.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        MainApplication.getComponent().plus(new RouteDetailsModule()).inject(this);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_details_activity);
        ButterKnife.bind(this);
        mBusRoute = Parcels.unwrap(getIntent().getParcelableExtra(BUS_ROUTE_ARG));
        initActionBar();
        initMap(savedInstanceState);
        initRecyclerView();
    }

    private void initActionBar() {
        setActionBar(mToolbar);
        setTitle(getString(R.string.route_details_activity_title, mBusRoute.getLine_code()));
        getActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initMap(Bundle savedInstanceState) {
        mapView.onCreate(savedInstanceState);
        mapView.getMapAsync(this);
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false);
        mRoutePartsRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(this, layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getDrawable(R.drawable.list_divider));
        mRoutePartsRecyclerView.addItemDecoration(dividerItemDecoration);

        BusRoutePartAdapter busAdapter = new BusRoutePartAdapter();
        mRoutePartsRecyclerView.setAdapter(busAdapter);
        mRoutePartsRecyclerView.setHasFixedSize(true);
        busAdapter.setData(mBusRoute.getRoute());
    }

    @Override
    public void onMapReady(GoogleMap map) {
        mapView.getViewTreeObserver().addOnGlobalLayoutListener(
                () -> map.moveCamera(mPresenter.buildMarkers(mBusRoute, map)));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onResume() {
        mapView.onResume();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

}
