package com.showcase.bus.schedule;

import com.showcase.bus.api.StationsEnum;
import com.showcase.bus.api.pojo.TimeTableResponse;
import com.showcase.bus.api.pojo.Timetable;

import io.reactivex.disposables.CompositeDisposable;

public class BusScheduleActivityPresenter {

    private final BusScheduleFeatures mBusScheduleFeatures;
    private final ScheduleBus mScheduleBus;

    private CompositeDisposable mCompositeDisposable;
    private IView mView;

    public BusScheduleActivityPresenter(BusScheduleFeatures busScheduleFeatures, ScheduleBus scheduleBus) {
        mBusScheduleFeatures = busScheduleFeatures;
        mScheduleBus = scheduleBus;
        mCompositeDisposable = new CompositeDisposable();
    }

    void startPresenting(IView view, StationsEnum stationsEnum) {
        this.mView = view;
        mCompositeDisposable.add(mBusScheduleFeatures.loadBusSchedule(stationsEnum)
                .subscribe(this::busScheduleLoadSuccess, this::busScheduleLoadFailed));
    }

    private void busScheduleLoadFailed(Throwable throwable) {
        mView.busScheduleLoadFailed();
    }

    private void busScheduleLoadSuccess(TimeTableResponse timeTableResponse) {
        Timetable timetable = timeTableResponse.getTimetable();
        mScheduleBus.setArrivalsList(timetable.getArrivals());
        mScheduleBus.setDeparturesList(timetable.getDepartures());
        mView.busScheduleLoadSuccess();
    }

    void stopPresenting() {
        mCompositeDisposable.clear();
    }

    interface IView {
        void busScheduleLoadSuccess();
        void busScheduleLoadFailed();
    }
}