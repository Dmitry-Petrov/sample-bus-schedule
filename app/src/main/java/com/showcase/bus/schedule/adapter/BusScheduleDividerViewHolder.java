package com.showcase.bus.schedule.adapter;

import static com.showcase.bus.schedule.adapter.BusScheduleAdapter.TYPE_DIVIDER;

import android.view.View;
import android.widget.TextView;

import com.showcase.bus.R;

import butterknife.BindView;

class BusScheduleDividerViewHolder extends BusScheduleItemViewHolder {

    @BindView(R.id.trip_day)
    TextView mTripDay;

    @BindView(R.id.arrival_or_departure_column)
    TextView mArrivalOrDepartureColumn;

    BusScheduleDividerViewHolder(View itemView) {
        super(itemView);
        mViewHolderType = TYPE_DIVIDER;
    }
}