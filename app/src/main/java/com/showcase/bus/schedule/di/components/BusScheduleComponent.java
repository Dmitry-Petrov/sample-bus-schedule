package com.showcase.bus.schedule.di.components;

import com.showcase.bus.di.qualifiers.PerActivity;
import com.showcase.bus.schedule.BusScheduleActivity;
import com.showcase.bus.schedule.BusScheduleFragment;
import com.showcase.bus.schedule.di.modules.BusScheduleModule;

import dagger.Subcomponent;

@Subcomponent(modules = {BusScheduleModule.class})
@PerActivity
public interface BusScheduleComponent {

    void inject(BusScheduleActivity busScheduleActivity);
    void inject(BusScheduleFragment busScheduleFragment);
}
