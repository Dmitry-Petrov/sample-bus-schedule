package com.showcase.bus.schedule;

import android.app.Fragment;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.showcase.bus.R;
import com.showcase.bus.api.pojo.BusRoute;
import com.showcase.bus.route.RouteDetailsActivity;
import com.showcase.bus.schedule.adapter.BusScheduleAdapter;

import org.parceler.Parcels;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;


public class BusScheduleFragment extends Fragment implements BusScheduleFragmentPresenter.IView {

    private static final String SCHEDULE_TYPE_ARG = "SCHEDULE_TYPE_ARG";

    @BindView(R.id.schedule_recycler_view)
    RecyclerView mScheduleRecyclerView;

    @Inject
    BusScheduleFragmentPresenter mPresenter;

    private ScheduleType mScheduleType;
    private BusScheduleAdapter mBusAdapter;

    public static BusScheduleFragment newInstance(ScheduleType scheduleType) {
        BusScheduleFragment fragment = new BusScheduleFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(SCHEDULE_TYPE_ARG, Parcels.wrap(scheduleType));
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        ((BusScheduleActivity) getActivity()).getBusScheduleComponent().inject(this);
        super.onCreate(savedInstanceState);
        mScheduleType = Parcels.unwrap(getArguments().getParcelable(SCHEDULE_TYPE_ARG));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.bus_schedule_fragment, container, false);
        ButterKnife.bind(this, view);
        initRecyclerView();
        return view;
    }

    private void initRecyclerView() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(),
                LinearLayoutManager.VERTICAL, false);
        mScheduleRecyclerView.setLayoutManager(layoutManager);
        DividerItemDecoration dividerItemDecoration = new DividerItemDecoration(getActivity(),
                layoutManager.getOrientation());
        dividerItemDecoration.setDrawable(getActivity().getDrawable(R.drawable.list_divider));
        mScheduleRecyclerView.addItemDecoration(dividerItemDecoration);
        mBusAdapter = new BusScheduleAdapter(mScheduleType);
        mBusAdapter.setOnRouteSelectedListener(busRoute -> RouteDetailsActivity.startActivity(getActivity(), busRoute));
        mScheduleRecyclerView.setAdapter(mBusAdapter);
        mScheduleRecyclerView.setHasFixedSize(true);
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.startPresenting(this, mScheduleType);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.stopPresenting();
    }

    @Override
    public void onRoutesLoaded(List<BusRoute> busRoutes) {
        mBusAdapter.setData(busRoutes);
    }
}
