package com.showcase.bus.schedule;

import static android.view.View.GONE;
import static android.view.View.VISIBLE;

import android.app.Activity;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.WindowManager;
import android.widget.ProgressBar;
import android.widget.Toolbar;

import com.showcase.bus.MainApplication;
import com.showcase.bus.R;
import com.showcase.bus.api.StationsEnum;
import com.showcase.bus.schedule.adapter.SectionsPagerAdapter;
import com.showcase.bus.schedule.di.components.BusScheduleComponent;
import com.showcase.bus.schedule.di.modules.BusScheduleModule;
import com.showcase.bus.utils.NetworkUtils;
import com.showcase.bus.utils.ViewUtils;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BusScheduleActivity extends Activity implements BusScheduleActivityPresenter.IView {

    private static final StationsEnum CURRENT_STATION = StationsEnum.MUNICH_ZOB;

    @BindView(R.id.view_pager)
    ViewPager mViewPager;

    @BindView(R.id.toolbar)
    Toolbar mToolbar;

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.progress_bar)
    ProgressBar mProgressBar;

    @Inject
    BusScheduleActivityPresenter mPresenter;

    private BusScheduleComponent mBusScheduleComponent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        injectDagger();
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bus_schedule_activity);
        ButterKnife.bind(this);
        initActionBar();
        initViewPager();
    }

    private void injectDagger() {
        mBusScheduleComponent = MainApplication.getComponent().plus(new BusScheduleModule());
        mBusScheduleComponent.inject(this);
    }

    private void initActionBar() {
        setActionBar(mToolbar);
        setTitle(CURRENT_STATION.getStationTitleId());
    }

    private void initViewPager() {
        mViewPager.setAdapter(new SectionsPagerAdapter(this, getFragmentManager()));
        mTabLayout.setupWithViewPager(mViewPager);
    }

    @Override
    protected void onStart() {
        super.onStart();
        loadBusSchedule();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.stopPresenting();
    }

    private void loadBusSchedule() {
        if (NetworkUtils.isNetworkAvailable(this)) {
            showProgressBar(true);
            mPresenter.startPresenting(this, CURRENT_STATION);
        } else {
            ViewUtils.displayErrorMessage(this, getString(R.string.error_no_network_title),
                    getString(R.string.error_no_network_message));
        }
    }

    @Override
    public void busScheduleLoadSuccess() {
        showProgressBar(false);
    }

    @Override
    public void busScheduleLoadFailed() {
        ViewUtils.displayErrorMessage(this, getString(R.string.error_unable_to_load_title),
                getString(R.string.error_unable_to_load_message));
        showProgressBar(false);
    }

    private void showProgressBar(boolean show) {
        if (show) {
            mProgressBar.setVisibility(VISIBLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                    WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        } else {
            mProgressBar.setVisibility(GONE);
            getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_bus_schedule, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_refresh:
                loadBusSchedule();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public BusScheduleComponent getBusScheduleComponent() {
        return mBusScheduleComponent;
    }

}
