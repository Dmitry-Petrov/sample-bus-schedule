package com.showcase.bus.schedule.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.showcase.bus.R;
import com.showcase.bus.schedule.ScheduleType;
import com.showcase.bus.api.pojo.BusRoute;
import com.showcase.bus.utils.DateConverter;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;


public class BusScheduleAdapter extends RecyclerView.Adapter<BusScheduleItemViewHolder> {

    static final int TYPE_ITEM = 1;
    static final int TYPE_DIVIDER = 2;

    private static final DateTimeFormatter TRIP_DAY_FORMAT = DateTimeFormat.forPattern("dd.MM.yyyy");
    private static final DateTimeFormatter ROUTE_DATE_FORMAT = DateTimeFormat.forPattern("HH:mm");

    private List<BusRoute> mBusRoutes = new ArrayList<>();

    private ScheduleType mScheduleType;

    private IOnRouteSelectedListener mOnRouteSelectedListener;

    public interface IOnRouteSelectedListener {
        void onRouteSelected(BusRoute selectedRoute);
    }

    public BusScheduleAdapter(ScheduleType scheduleType) {
        mScheduleType = scheduleType;
    }

    public void setData(List<BusRoute> busRoutes) {
        this.mBusRoutes = busRoutes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return TYPE_DIVIDER;
        }
        DateTime currentRoute = DateConverter.toJodaDateTime(mBusRoutes.get(position).getDatetime());
        DateTime previousRoute = DateConverter.toJodaDateTime(mBusRoutes.get(position - 1).getDatetime());
        return currentRoute.getDayOfYear() == previousRoute.getDayOfYear() ? TYPE_ITEM : TYPE_DIVIDER;
    }

    @Override
    public BusScheduleItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        if (viewType == TYPE_ITEM) {
            return new BusScheduleItemViewHolder(inflater.inflate(R.layout.schedule_item, parent, false));
        } else {
            return new BusScheduleDividerViewHolder(inflater.inflate(R.layout.schedule_item_and_divider, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(final BusScheduleItemViewHolder holder, final int position) {
        BusRoute busRoute = mBusRoutes.get(position);
        holder.mLineCode.setText(busRoute.getLine_code());
        holder.mDirection.setText(busRoute.getDirection());
        holder.mArrivalOrDepartureTime.setText(formatArrivalOrDepartureTime(busRoute));
        if (holder.getViewHolderType() == TYPE_DIVIDER) {
            BusScheduleDividerViewHolder dividerViewHolder = (BusScheduleDividerViewHolder) holder;
            dividerViewHolder.mTripDay.setText(formatTripDay(busRoute));
            dividerViewHolder.mArrivalOrDepartureColumn.setText(
                    mScheduleType == ScheduleType.ARRIVALS ? R.string.bus_column_arrivals : R.string.bus_column_departures);
        }
        holder.mRouteItemRoot.setOnClickListener(
                v -> mOnRouteSelectedListener.onRouteSelected(mBusRoutes.get(holder.getAdapterPosition())));
    }

    @Override
    public int getItemCount() {
        return mBusRoutes.size();
    }

    public void setOnRouteSelectedListener(IOnRouteSelectedListener onRouteSelectedListener) {
        this.mOnRouteSelectedListener = onRouteSelectedListener;
    }

    private String formatTripDay(BusRoute busRoute) {
        return TRIP_DAY_FORMAT.print(DateConverter.toJodaDateTime(busRoute.getDatetime()));
    }

    private String formatArrivalOrDepartureTime(BusRoute busRoute) {
        return ROUTE_DATE_FORMAT.print(DateConverter.toJodaDateTime(busRoute.getDatetime()));
    }
}
