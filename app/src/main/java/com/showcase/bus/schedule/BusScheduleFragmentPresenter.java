package com.showcase.bus.schedule;

import android.support.annotation.NonNull;

import com.showcase.bus.api.pojo.BusRoute;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.disposables.CompositeDisposable;

public class BusScheduleFragmentPresenter {

    private ScheduleBus mScheduleBus;

    private CompositeDisposable mCompositeDisposable;

    public BusScheduleFragmentPresenter(ScheduleBus scheduleBus) {
        mScheduleBus = scheduleBus;
        mCompositeDisposable = new CompositeDisposable();
    }

    void startPresenting(IView view, ScheduleType scheduleType) {
        Observable<List<BusRoute>> observable = null;
        switch (scheduleType) {
            case ARRIVALS:
                observable = mScheduleBus.onArrivalsListAvailable();
                break;
            case DEPARTURES:
                observable = mScheduleBus.onDeparturesListAvailable();
                break;
        }
        mCompositeDisposable.add(observable.map(this::sortByTimeStamp).subscribe(view::onRoutesLoaded));
    }

    @NonNull
    private List<BusRoute> sortByTimeStamp(List<BusRoute> unsortedList) {
        /*The idea of this approach is that we don't modify underlying array, so we don't introduce
         unwanted behaviour to other users of this observable.*/
        List<BusRoute> sortedList = new ArrayList<>(unsortedList);
        Collections.sort(sortedList,
                (o1, o2) -> o1.getDatetime().getTimestamp().compareTo(o2.getDatetime().getTimestamp()));
        return sortedList;
    }

    void stopPresenting() {
        mCompositeDisposable.clear();
    }

    interface IView {
        void onRoutesLoaded(List<BusRoute> busRoutes);
    }
}