package com.showcase.bus.schedule.adapter;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.support.v13.app.FragmentPagerAdapter;

import com.showcase.bus.R;
import com.showcase.bus.schedule.BusScheduleFragment;
import com.showcase.bus.schedule.ScheduleType;

public class SectionsPagerAdapter extends FragmentPagerAdapter {

    private final Context mContext;

    public SectionsPagerAdapter(Context context, FragmentManager fm) {
        super(fm);
        mContext = context;
    }

    @Override
    public Fragment getItem(int position) {
        return BusScheduleFragment.newInstance((position == 0 ? ScheduleType.ARRIVALS : ScheduleType.DEPARTURES));
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return mContext.getString(R.string.arrivals_tab);
            case 1:
                return mContext.getString(R.string.departures_tab);
        }
        return null;
    }
}