package com.showcase.bus.schedule.adapter;

import static com.showcase.bus.schedule.adapter.BusScheduleAdapter.TYPE_ITEM;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.showcase.bus.R;

import butterknife.BindView;
import butterknife.ButterKnife;

class BusScheduleItemViewHolder extends RecyclerView.ViewHolder {

    @BindView(R.id.route_item_root)
    ViewGroup mRouteItemRoot;

    @BindView(R.id.line_code)
    TextView mLineCode;

    @BindView(R.id.direction)
    TextView mDirection;

    @BindView(R.id.arrival_or_departure_time)
    TextView mArrivalOrDepartureTime;

    int mViewHolderType;

    BusScheduleItemViewHolder(View itemView) {
        super(itemView);
        mViewHolderType = TYPE_ITEM;
        ButterKnife.bind(this, itemView);
    }

    int getViewHolderType() {
        return mViewHolderType;
    }
}