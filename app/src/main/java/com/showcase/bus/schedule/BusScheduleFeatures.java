package com.showcase.bus.schedule;

import com.showcase.bus.api.BusRouteApi;
import com.showcase.bus.api.RetrofitUtils;
import com.showcase.bus.api.StationsEnum;
import com.showcase.bus.api.pojo.TimeTableResponse;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class BusScheduleFeatures {

    Single<TimeTableResponse> loadBusSchedule(StationsEnum stationToLoad) {
        BusRouteApi busRouteApi = RetrofitUtils.getService(BusRouteApi.class);
        return busRouteApi.loadBusScheduleForStation(stationToLoad.getStationId()).observeOn(
                AndroidSchedulers.mainThread()).subscribeOn(Schedulers.io());
    }

}
