package com.showcase.bus.schedule.di.modules;

import com.showcase.bus.schedule.BusScheduleActivityPresenter;
import com.showcase.bus.schedule.BusScheduleFeatures;
import com.showcase.bus.schedule.BusScheduleFragmentPresenter;
import com.showcase.bus.schedule.ScheduleBus;

import dagger.Module;
import dagger.Provides;

@Module
public class BusScheduleModule {

    @Provides
    BusScheduleFeatures provideBusScheduleFeatures() {
        return new BusScheduleFeatures();
    }

    @Provides
    BusScheduleActivityPresenter provideBusScheduleActivityPresenter(BusScheduleFeatures busScheduleFeatures, ScheduleBus scheduleBus) {
        return new BusScheduleActivityPresenter(busScheduleFeatures, scheduleBus);
    }

    @Provides
    BusScheduleFragmentPresenter provideBusScheduleFragmentPresenter(ScheduleBus scheduleBus) {
        return new BusScheduleFragmentPresenter(scheduleBus);
    }

}
