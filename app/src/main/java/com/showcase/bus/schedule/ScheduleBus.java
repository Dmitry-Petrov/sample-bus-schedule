package com.showcase.bus.schedule;

import com.showcase.bus.api.pojo.BusRoute;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;

public class ScheduleBus {

    private BehaviorSubject<List<BusRoute>> mArrivalsListSubject = BehaviorSubject.create();
    private BehaviorSubject<List<BusRoute>> mDeparturesListSubject = BehaviorSubject.create();

    void setArrivalsList(List<BusRoute> arrivalsList) {
        mArrivalsListSubject.onNext(arrivalsList);
    }
    Observable<List<BusRoute>> onArrivalsListAvailable() {
        return mArrivalsListSubject;
    }

    void setDeparturesList(List<BusRoute> departuresList) {
        mDeparturesListSubject.onNext(departuresList);
    }
    Observable<List<BusRoute>> onDeparturesListAvailable() {
        return mDeparturesListSubject;
    }

}
