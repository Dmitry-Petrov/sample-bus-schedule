package com.showcase.bus.schedule;

import org.parceler.Parcel;

@Parcel
public enum ScheduleType {
    ARRIVALS, DEPARTURES
}