package com.showcase.bus.utils;

import android.support.annotation.NonNull;

import com.showcase.bus.api.pojo.Datetime;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

import java.util.TimeZone;

public class DateConverter {

    /**
     * Converts Datetime object, received from server to Joda's DateTime
     * @param datetime server's Datettime json POJO
     * @return Joda's DateTime
     */
    @NonNull
    public static DateTime toJodaDateTime(Datetime datetime) {
        return new DateTime(datetime.getTimestamp() * 1000, DateTimeZone.forTimeZone(TimeZone.getTimeZone(datetime.getTz())));
    }

}
