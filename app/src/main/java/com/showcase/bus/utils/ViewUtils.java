package com.showcase.bus.utils;

import android.content.Context;
import android.support.v7.app.AlertDialog;

import com.showcase.bus.R;

public class ViewUtils {

    public static void displayErrorMessage(Context context, String title, String message) {
        AlertDialog alertDialog = new AlertDialog.Builder(context).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, context.getString(R.string.ok_button),
                (dialog, which) -> dialog.dismiss());
        alertDialog.show();
    }
}
