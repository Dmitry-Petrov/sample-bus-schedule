package com.showcase.bus.utils;

import android.content.Context;

public class DimensionUtils {

    public static int fromDpToPixels(final Context context, final int dp) {
        return (int) (dp * context.getResources().getDisplayMetrics().density);
    }
}
