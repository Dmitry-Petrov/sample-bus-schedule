package com.showcase.bus.di.modules;

import android.content.Context;

import com.showcase.bus.schedule.ScheduleBus;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final Context mAppContext;

    public ApplicationModule(Context context) {
        this.mAppContext = context.getApplicationContext();
    }

    @Singleton
    @Provides
    Context getApplicationContext() {
        return mAppContext;
    }

    @Provides
    @Singleton
    ScheduleBus provideScheduleBus() {
        return new ScheduleBus();
    }

}
