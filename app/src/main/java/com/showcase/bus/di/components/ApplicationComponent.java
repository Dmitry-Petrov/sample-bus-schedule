package com.showcase.bus.di.components;

import com.showcase.bus.MainApplication;
import com.showcase.bus.di.modules.ApplicationModule;
import com.showcase.bus.route.di.components.RouteDetailsComponent;
import com.showcase.bus.route.di.modules.RouteDetailsModule;
import com.showcase.bus.schedule.di.components.BusScheduleComponent;
import com.showcase.bus.schedule.di.modules.BusScheduleModule;

import javax.inject.Singleton;

import dagger.Component;

@Component(modules = {ApplicationModule.class,})
@Singleton
public interface ApplicationComponent {

    void inject(MainApplication application);

    BusScheduleComponent plus(BusScheduleModule busScheduleModule);

    RouteDetailsComponent plus(RouteDetailsModule routeDetailsModule);
}
