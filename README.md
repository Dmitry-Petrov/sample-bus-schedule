# sample-bus-schedule  
  
A simple Bus Schedule application, main purpose of which is to practice in using MVP architecture with modern libraries like **RxAndroid, Dagger 2, Retrofit 2** and so on.  
  
Consists of:  
1.	Main screen displaying arrivals and departures.  
2.	Route details screen displaying all legs of the journey on Google Maps.  
